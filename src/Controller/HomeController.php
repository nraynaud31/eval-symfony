<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Products;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ManagerRegistry $doctrine, PaginatorInterface $paginator, EntityManagerInterface $em, Request $request): Response
    {
        // $products = $doctrine->getRepository(Products::class)->findAll();
        $dql = "SELECT a FROM App:Products a ORDER BY a.id DESC";
        $query = $em->createQuery($dql);
    
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        // Formulaire de recherche
        $form = $this->createFormBuilder()
        ->setAction($this->generateUrl('handleSearch'))
        ->add('query', TextType::class, [
            'label' => false,
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Chercher un produit...'
            ]
        ])
        ->add('recherche', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-primary col-md-12'
            ]
        ])
        ->getForm();

        return $this->render('home/index.html.twig', [
            'pagination' => $pagination,
            'formRender' => $form->createView(),
        ]);
    }

    #[Route('/handleSearch', name: 'handleSearch')]
    public function handleSearch(Request $request, ProductsRepository $repo)
    {
        $query = $request->request->all('form')["query"]; 
        if($query) {

            // On récupère le nom entré dans le formulaire de recherche pour lancer la recherche
            $produits = $repo->findProductsByName($query);
        }
        return $this->render('searchResult/index.html.twig', [
            'produits' => $produits
        ]);
    }

}
