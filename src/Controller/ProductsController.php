<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Products;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\CreateProductType;


class ProductsController extends AbstractController
{
    #[Route('/products', name: 'app_products')]
    public function index(ManagerRegistry $doctrine, EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {
        $dql = "SELECT a FROM App:Products a ORDER BY a.id DESC";
        $query = $em->createQuery($dql);
    
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('products/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }


    #[Route('/product/{id}', name: 'app_detail_products')]
    public function indexProductId(int $id, ManagerRegistry $doctrine): Response
    {
        $products = $doctrine->getRepository(Products::class)->find($id);
        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route('/admin/products', name: 'app_admin_products')]
    public function indexAdmin(ManagerRegistry $doctrine): Response
    {
        $products = $doctrine->getRepository(Products::class)->findAll();

        return $this->render('admin_products/index.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route('/admin/products/add', name: 'app_products_admin_add')]
    public function adminProductsAdd(Request $request,EntityManagerInterface $entityManager): Response
    {
        $newProduct = new Products();
        $form = $this->createForm(CreateProductType::class, $newProduct);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $image = $form->get('image_link')->getData();
            
            // On hash le nom de l'image pour éviter des conflits d'intérêt
            $fichier = md5(uniqid()). '.'.$image->guessExtension();

            // On stocke l'image dans le dossier public/uploads
            $image->move(
                $this->getParameter("images_directory"),
                $fichier
            );

            // On stocke l'image en DB
            $newProduct->setImageLink('../img/'.$fichier);
            $entityManager->persist($newProduct);
            $entityManager->flush();
            return $this->redirectToRoute('app_admin_products');
        }

        return $this->render('products_admin_add/index.html.twig', [
            'formRender' => $form->createView(),
        ]);
    }

    #[Route('/admin/products/update/{id}', name: 'app_products_update_add')]
    public function adminProductUpdate(ManagerRegistry $doctrine, int $id, Request $request,EntityManagerInterface $entityManager): Response
    {
        $newProduct = $doctrine->getRepository(Products::class)->find($id);
        $form = $this->createForm(CreateProductType::class, $newProduct);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $image = $form->get('image_link')->getData();

            // On supprime l'ancienne image
            $exploding = explode(".." ,$newProduct->getImageLink());
            unlink('.'.$exploding[1]);
            
            // On hash le nom de l'image pour éviter des conflits d'intérêt
            $fichier = md5(uniqid()). '.'.$image->guessExtension();

            // On stocke l'image dans le dossier public/uploads
            $image->move(
                $this->getParameter("images_directory"),
                $fichier
            );

            // On stocke l'image en DB
            $newProduct->setImageLink('../img/'.$fichier);
            $entityManager->persist($newProduct);
            $entityManager->flush();
            return $this->redirectToRoute('app_admin_products');
        }

        return $this->render('products_admin_add/index.html.twig', [
            'formRender' => $form->createView(),
        ]);
    }

    #[Route('/admin/products/delete/{id}', name: 'app_admin_products_delete')]
    public function adminProductDelete(ManagerRegistry $doctrine, int $id, Request $request,EntityManagerInterface $entityManager): Response
    {
        $Products = $doctrine->getRepository(Products::class)->find($id);
        $em = $doctrine->getManager();
        if($Products->getTags() != null){
            $tags = $Products->getTags();
            // On supprime tous les tags qui existent
            foreach ($tags as $tag) {
                $Products->removeTag($tag);
                $em->remove($tag);
            }
        }
        // On supprime le fichier de l'image
        $exploding = explode(".." ,$Products->getImageLink());
        unlink('.'.$exploding[1]);
        $em->remove($Products);
        $em->flush();

        return $this->redirectToRoute('app_admin_products');
    }
}
