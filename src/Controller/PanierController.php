<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Panier;

class PanierController extends AbstractController
{
    #[Route('/panier', name: 'app_panier')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $paniers = $doctrine->getRepository(Panier::class)->findAll();
        foreach ($paniers as $panier) {
            $user = $panier->getUser();
            $products = $panier->getProduits();
        }
        
        return $this->render('panier/index.html.twig', [
            'products' => $products,
        ]);
    }
}
