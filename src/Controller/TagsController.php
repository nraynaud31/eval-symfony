<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Tags;
use App\Form\CreateTagsType;

class TagsController extends AbstractController
{
    #[Route('/admin/tags', name: 'app_admin_tags')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $tags = $doctrine->getRepository(Tags::class)->findAll();
        
        return $this->render('admin_tags/index.html.twig', [
            'tags' => $tags,
        ]);
    }

    #[Route('/admin/tags/add', name: 'app_tags_admin_add')]
    public function adminTagsAdd(Request $request,EntityManagerInterface $entityManager): Response
    {
        $newTags = new Tags();
        $form = $this->createForm(CreateTagsType::class, $newTags);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){ 
            $produits = $form->get('produits')->getData();
            foreach ($produits as $produit) {
                // On ajoute tous les produits choisis
                $newTags->addProduit($produit);
            }
            $entityManager->persist($newTags);
            $entityManager->flush();
            return $this->redirectToRoute('app_admin_tags');
        }

        return $this->render('tags_admin_add/index.html.twig', [
            'formRender' => $form->createView(),
        ]);
    }

    #[Route('/admin/tags/update/{id}', name: 'app_tags_admin_update')]
    public function adminTagsUpdate(int $id, ManagerRegistry $doctrine, Request $request,EntityManagerInterface $entityManager): Response
    {
        $newTags = $doctrine->getRepository(Tags::class)->find($id);
        $form = $this->createForm(CreateTagsType::class, $newTags);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $produit_exist = $newTags->getProduits();
            // On récupère les produits existants pour les mettre à jour
            if($newTags->getProduits()){
                foreach ($produit_exist as $produit) {
                    $newTags->removeProduit($produit);
                }
            }
            
            $produits = $form->get('produits')->getData();
            foreach ($produits as $produit) {
                // On ajoute tous les produits choisis
                $newTags->addProduit($produit);
            }
            $entityManager->persist($newTags);
            $entityManager->flush();
            return $this->redirectToRoute('app_admin_tags');
        }

        return $this->render('tags_admin_add/index.html.twig', [
            'formRender' => $form->createView(),
        ]);
    }

    #[Route('/admin/tags/delete/{id}', name: 'app_admin_tags_delete')]
    public function adminProductDelete(ManagerRegistry $doctrine, int $id, Request $request,EntityManagerInterface $entityManager): Response
    {
        $tags = $doctrine->getRepository(Tags::class)->find($id);
        $em = $doctrine->getManager();
        $em->remove($tags);
        $em->flush();

        return $this->redirectToRoute('app_admin_tags');
    }
}
