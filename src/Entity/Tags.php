<?php

namespace App\Entity;

use App\Repository\TagsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TagsRepository::class)]
class Tags
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    private $name;

    #[ORM\ManyToMany(targetEntity: Products::class, mappedBy: 'tags')]
    private $produits;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->produits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString(): string
    {
    return $this->getName();  // or some string field in your Vegetal Entity 
    }

    /**
     * @return Collection<int, Products>
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addProduit(Products $produit): self
    {
        if (!$this->produits->contains($produit)) {
            $this->produits[] = $produit;
            $produit->addTag($this);
        }

        return $this;
    }

    public function removeProduit(Products $produit): self
    {
        if ($this->produits->removeElement($produit)) {
            $produit->removeTag($this);
        }

        return $this;
    }
}
